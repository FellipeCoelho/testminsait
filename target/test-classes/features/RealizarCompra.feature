@smoke
Feature: Realizar compra de vestido casual

  Background: Produto escolhido e login previamente
    Given estou no site
    And quero comprar um produto

  Scenario: Compra realizada com cheque
    When escolho pagar com cheque
    Then meu pedido sera concluido

  Scenario: Compra realizada com tranferencia bancaria
    When escolho pagar com tranferencia
    Then meu pedido sera finalizado
    
  Scenario: Tentativa de compra com termos de servi�oes desmarcados
  	And volto para tela de entrega
  	When prossigo com o pedido
  	Then meu pedido nao sera finalizado
  	
  	
