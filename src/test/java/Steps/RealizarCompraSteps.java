package Steps;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.*;

import io.github.bonigarcia.wdm.WebDriverManager;
import pages.CarrinhoComprasPage;
import pages.ConfirmacaoPedidoPage;
import pages.EnderecosPage;
import pages.EnvioPage;
import pages.LoginPage;
import pages.MetodoDePagamentoPage;
import pages.MinhaContaPage;
import pages.PagamentoChequePage;
import pages.PagamentoTranferenciaPage;
import pages.ResultadoBuscaPage;


public class RealizarCompraSteps {
		
	WebDriver driver = null;
	
	@Given("estou no site")
	public void estou_no_site() {
		
		WebDriverManager.chromedriver().driverVersion("106.0.5249.9").setup();
		
		driver = new ChromeDriver();

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		driver.manage().timeouts().implicitlyWait(Duration.ofMinutes(2));
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(20));
		
		driver.manage().window().maximize();
		
		driver.navigate().to("http://automationpractice.com/index.php?id_category=9&controller=category");
	}
	
	@Given("quero comprar um produto")
	public void quero_comprar_um_produto() {
		
		ResultadoBuscaPage busca = new ResultadoBuscaPage(driver);
		CarrinhoComprasPage cart = new CarrinhoComprasPage(driver);
		LoginPage login = new LoginPage(driver);
		MinhaContaPage mainAcc = new MinhaContaPage(driver);
		EnderecosPage endereco = new EnderecosPage(driver);
		EnvioPage envio = new EnvioPage(driver);
		
		busca.addProdutoAoCarrinho();
		cart.ClicarEmLogin();
		login.fazerLogin();
		mainAcc.ProsseguirCheckout();
		cart.ProsseguirParaCheckout();
		endereco.ProsseguirParaCheckout();
		envio.ConcordarComTermos();
		envio.ProsseguirParaCheckout();


	}

	@When("escolho pagar com cheque")
	public void escolho_pagar_com_cheque() {
		
		MetodoDePagamentoPage pagamento = new MetodoDePagamentoPage(driver);
		PagamentoChequePage cheque = new PagamentoChequePage(driver);
		
		pagamento.PagarComCheque();
		cheque.ConfirmarPedido();
	}

	@When("escolho pagar com tranferencia")
	public void escolho_pagar_com_tranferencia() {
		
		MetodoDePagamentoPage pagamento = new MetodoDePagamentoPage(driver);
		PagamentoTranferenciaPage transferencia = new PagamentoTranferenciaPage(driver);
		
		pagamento.PagarComTranferencia();
		transferencia.ConfirmarPedido();

	}
	
	@Then("meu pedido sera concluido")
	public void meu_pedido_sera_concluido() {
		
		ConfirmacaoPedidoPage confirma = new ConfirmacaoPedidoPage(driver);
		
		confirma.PedidoConfirmado();

	}
	
	
	@And("volto para tela de entrega")
	public void volto_para_tela_de_entrega() {
		
		MetodoDePagamentoPage pagamento = new MetodoDePagamentoPage(driver);
	
		pagamento.VoltarParaEntrega();

	}
	
	@When("prossigo com o pedido")
	public void prossigo_com_o_pedido() {
		
		EnvioPage envio = new EnvioPage(driver);

		envio.ProsseguirParaCheckout();

	}
	
	@Then("meu pedido sera finalizado")
	public void meu_pedido_sera_finalizado() {
		
		ConfirmacaoPedidoPage confirma = new ConfirmacaoPedidoPage(driver);
		
		confirma.PedidoFinalizado();

	}
	
	@Then("meu pedido nao sera finalizado")
	public void meu_pedido_nao_sera_finalizado() {
		
		EnvioPage envio = new EnvioPage(driver);
		
		envio.ApareceAlerta();

	}
}
