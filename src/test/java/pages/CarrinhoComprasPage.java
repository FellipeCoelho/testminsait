package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CarrinhoComprasPage {
	
	WebDriver driver;

	@FindBy(className = "login")
	WebElement botaoLogin;
	
	@FindBy(css = "[class='button btn btn-default standard-checkout button-medium']")
	WebElement botaoCheckout;
	
	public CarrinhoComprasPage(WebDriver driver) {
		
		this.driver = driver;		
		PageFactory.initElements(driver, this);
	}
	
	public void ClicarEmLogin() {
		
		botaoLogin.click();
		
	}
	
	public void ProsseguirParaCheckout() {
		
		LocalizarElemento(botaoCheckout);
		botaoCheckout.click();
		
	}
	
	private void LocalizarElemento(WebElement elemento) {
		Actions actions = new Actions(driver);
		actions.moveToElement(elemento);
		actions.perform();
	}
	
}
