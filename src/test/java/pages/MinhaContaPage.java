package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MinhaContaPage {
	
	WebDriver driver;

	@FindBy(css = "[title='View my shopping cart']")
	WebElement carrinho;
	
	@FindBy(id = "button_order_cart")
	WebElement botaoCheckout;
	
	public MinhaContaPage(WebDriver driver) {
		
		this.driver = driver;		
		PageFactory.initElements(driver, this);
	}
	
	public void ProsseguirCheckout() {
		
		Actions actions = new Actions(driver);
		actions.moveToElement(carrinho);
		actions.perform();
		botaoCheckout.click();
	}
}
