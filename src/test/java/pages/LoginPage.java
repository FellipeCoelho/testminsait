package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	
	WebDriver driver;

	@FindBy(id = "email")
	WebElement emailLogin ;
	
	@FindBy(id = "passwd")
	WebElement senhaLogin;
	
	@FindBy(id = "SubmitLogin")
	WebElement botaoLogin;
	
	public LoginPage(WebDriver driver) {
		
		this.driver = driver;		
		PageFactory.initElements(driver, this);
	}
	
	public void fazerLogin() {
		
		String email = "polloski.ignco@gmail.com";
		String senha = "testes5589";
		
		emailLogin.sendKeys(email);
		senhaLogin.sendKeys(senha);
		botaoLogin.click();
	}
	
}
