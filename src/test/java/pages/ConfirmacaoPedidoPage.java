package pages;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConfirmacaoPedidoPage {
	
	WebDriver driver;

	@FindBy(css = "[class='alert alert-success']")
	WebElement alertaSucesso;
	
	@FindBy(className = "cheque-indent")
	WebElement confirmacao2;
	
	public ConfirmacaoPedidoPage(WebDriver driver) {
		
		this.driver = driver;		
		PageFactory.initElements(driver, this);
	}
	
	public void PedidoConfirmado() {
	
		assertEquals(ApareceAlerta(),true);
	}
	
	public void PedidoFinalizado() {
		
		String texto = confirmacao2.getText();
		assertEquals(texto,"Your order on My Store is complete.");
	}
	
	private boolean ApareceAlerta() {
		
		return alertaSucesso.isDisplayed();
	}
	
}
