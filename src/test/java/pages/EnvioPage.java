package pages;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EnvioPage {
	
	WebDriver driver;

	@FindBy(id = "cgv")
	WebElement checkboxTermos;
	
	@FindBy(css = "[class='button btn btn-default standard-checkout button-medium']")
	WebElement botaoProsseguir;
	
	@FindBy(className = "fancybox-error")
	WebElement alertaErro;
	
	public EnvioPage(WebDriver driver) {
		
		this.driver = driver;		
		PageFactory.initElements(driver, this);
	}
	
	public void ConcordarComTermos() {
		
		checkboxTermos.click();
		
	}
	
	public void ApareceAlerta() {
	
		String alerta = alertaErro.getText();
		assertEquals(alerta,"You must agree to the terms of service before continuing.");
	}
	
	public void ProsseguirParaCheckout() {
		botaoProsseguir.click();
	}

}
