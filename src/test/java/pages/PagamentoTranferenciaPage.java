package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PagamentoTranferenciaPage {
	
	WebDriver driver;

	@FindBy(css = "[class='button btn btn-default button-medium']")
	WebElement botaoConfirmarPedido;
	
	public PagamentoTranferenciaPage(WebDriver driver) {
		
		this.driver = driver;		
		PageFactory.initElements(driver, this);
	}
	
	public void ConfirmarPedido() {
		
		botaoConfirmarPedido.click();
	}
}
