package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EnderecosPage{
	
	WebDriver driver;

	@FindBy(css = "[name='processAddress']")
	WebElement botaoCheckout;
	
	public EnderecosPage(WebDriver driver) {
		
		this.driver = driver;		
		PageFactory.initElements(driver, this);
	}
	
	public void ProsseguirParaCheckout() {
		
		LocalizarElemento(botaoCheckout);
		botaoCheckout.click();
	}
	
	private void LocalizarElemento(WebElement elemento) {
		Actions actions = new Actions(driver);
		actions.moveToElement(elemento);
		actions.perform();
	}
}
