package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MetodoDePagamentoPage {
	
	WebDriver driver;

	@FindBy(className = "cheque")
	WebElement cheque;
	
	@FindBy(className = "bankwire")
	WebElement tranferencia;
	
	@FindBy(css = "[class='button-exclusive btn btn-default']")
	WebElement botaoVoltar;
		
	public MetodoDePagamentoPage(WebDriver driver) {
		
		this.driver = driver;		
		PageFactory.initElements(driver, this);
	}
	
	public void PagarComCheque() {
		
		LocalizarElemento(cheque);
		cheque.click();
		
	}
	
	public void PagarComTranferencia() {
		
		tranferencia.click();
	}
	
	public void VoltarParaEntrega() {
		
		LocalizarElemento(botaoVoltar);
		botaoVoltar.click();
		
	}
	
	private void LocalizarElemento(WebElement elemento) {
		Actions actions = new Actions(driver);
		actions.moveToElement(elemento);
		actions.perform();
	}
}
