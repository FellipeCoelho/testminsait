package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ResultadoBuscaPage {
	
	WebDriver driver;

	@FindBy(className = "product-container")
	WebElement containerProduto;
	
	@FindBy(css = "[class = 'button ajax_add_to_cart_button btn btn-default']")
	WebElement addAoCarrinho;
	
	@FindBy(css = "[title = 'Proceed to checkout']")
	WebElement checkoutBotao;
	
	public ResultadoBuscaPage(WebDriver driver) {
		
		this.driver = driver;		
		PageFactory.initElements(driver, this);
	}

	public void addProdutoAoCarrinho(){
		
		Actions actions = new Actions(driver);
		actions.moveToElement(containerProduto);
		actions.perform();
		addAoCarrinho.click();
		checkoutBotao.click();
	}
	
}
